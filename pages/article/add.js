let App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_id: "",     // 用户id
    list:{},        //分类列表信息
    hasLogin:false,
    files: []  //图片上传参数数组,一个是路径，一个是图片id
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showToast({
      icon: 'loading',
    })
    let _this = this;
    
    if (App.globalData.hasLogin == false){
     return;  
    }  
    _this.setData({ 
      hasLogin: App.globalData.hasLogin ,
      user_id: App.globalData.user_id
    });

    _this.getarticleDetail();      //获取分类列表信息

    wx.hideToast();  //隐藏Toast
  },

 

  /**
   * 获取列表信息信息 */
  
  getarticleDetail: function () {
    let _this = this;
    App._get('api/arti_add', {
    }, function (result) {
      // 初始化信息详情数据
      let data = result.data;
      _this.setData(data);
    });
  },
  

  /*授权登录*/
  login: function (e) {
    let _this = this;
    if (e.detail.errMsg !== 'getUserInfo:ok') {
      return false;
    }
    wx.showLoading({ title: "正在登录", mask: true });
    // 执行微信登录
    wx.login({
      success: function (res) {
        // 发送用户信息
        App._post_form('api/user_login'
          , {
            code: res.code,
            user_info: e.detail.rawData,
            encrypted_data: e.detail.encryptedData,
            iv: e.detail.iv,
            signature: e.detail.signature
          }
          , function (result) {
            // 记录token user_id
            wx.setStorageSync('token', result.data.token);
            wx.setStorageSync('user_id', result.data.user_id);
            App.globalData.user_id = result.data.user_id;
            App.globalData.hasLogin = true;
            _this.setData({
              hasLogin: App.globalData.hasLogin,
              user_id: App.globalData.user_id
            });

            // 再通过用户信息获取详情
            _this.getarticleDetail();
          }
          , false
          , function () {
            wx.hideLoading();
          });
      }
    });
  },


  /*上传图片，按键方法*/
  chooseImage: function (e) {
    let that = this;
    let imgfiles = that.data.files;
    console.log(imgfiles.length);
    wx.chooseImage({
      count: 5 - imgfiles.length,    //最多选取5张,这里微信后台会控制
      sizeType: ['compressed'], // 传压缩图
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        let imgsrc = res.tempFilePaths;
       // imgfiles = imgfiles.concat(imgsrc);
       
        that.uploadimg({
          url: App.uploadimgRoot,              //这里是你图片上传的接口
          path: imgsrc                         //这里是选取的图片的地址数组
          });   //多张图上传到服务器
        //that.upload(that, res.tempFilePaths);          /*上传到服务器*/
       
      }
    })
   
  },
  //预览按钮回调
  previewImage: function (e) {
    let _this = this;
    let data = _this.data.files;
    let imgurls = [];
    for (var i = 0; i < data.length; i++) {
      imgurls = imgurls.concat(data[i].file_path);
    }
    wx.previewImage({
      urls: imgurls // 需要预览的图片http链接列表
    })
  },

 

  //多张图片上传
  uploadimg: function(data){
    wx.showToast({
      icon: "loading",
      title: "正在上传"
    });
    var that = this,
    i=data.i ? data.i : 0,//当前上传的哪张图片
    success=data.success ? data.success : 0,//上传成功的个数
    fail=data.fail ? data.fail : 0;//上传失败的个数
    
    wx.uploadFile({
      url: data.url,
      filePath: data.path[i],
      name: 'iFile',//这里根据自己的实际情况改
      header: { "Content-Type": "multipart/form-data" },
      formData: {
        group_id: 3,    //这里后台都传到文章图片类了
        wxapp_id: App.siteInfo.uniacid
      },
      success: (resp) => {
        let res = JSON.parse(resp.data);
        if(res.code==1){
          success++;//图片上传成功，图片上传成功的变量+1
          let imgfile = res.data;
          that.setData({
            files: that.data.files.concat(imgfile)     //上传成功后本地加图片
          });
        }
      },
      fail: (res) => {
        fail++;//图片上传失败，图片上传失败的变量+1
        console.log('fail:' + i + "fail:" + fail);
      },
      complete: () => {
        
        i++;//这个图片执行完上传后，开始上传下一张
        if (i == data.path.length) {   //当图片传完时，停止调用          
          wx.hideToast();  //隐藏Toast
          //console.log('执行完毕');
          //console.log('成功：' + success + " 失败：" + fail);
        } else {//若图片还没有传完，则继续调用函数
          data.i = i;
          data.success = success;
          data.fail = fail;
          that.uploadimg(data);    //递归调用上传
        }
      }
    });
  },







  /*表单提交*/
  formSubmit(e){
    console.log(e);
    let _that = this;
   

    if (e.detail.value.name == "") return wx.showModal({
      title: '提示',
      content: '店名或者事项名不能为空',
      showCancel: false
    })
    if (e.detail.value.content == "") return wx.showModal({
      title: '提示',
      content: '介绍不能为空',
      showCancel: false
    })
    if (e.detail.value.category_id == "") return wx.showModal({
      title: '提示',
      content: '请选择发布信息类型',
      showCancel: false
    })
    if (e.detail.value.tel == "") return wx.showModal({
      title: '提示',
      content: '电话不能为空',
      showCancel: false
    })

    let formeDate = e.detail.value;
    formeDate['form_id'] = e.detail.formId;      //模板消息参数
    //发送文章
    App._post_form('api/arti_add'
      , formeDate
      , function (result) {
        if(result.code==1){
          wx.showModal({
            title: '温馨提示',
            content: result.data,
            showCancel: false,
            success: function (res) {
              //下面数据清空就是清空表单
              _that.setData(
                {
                  formdata:"",
                  files:[]        
                }
              )
              if (res.confirm) {
                wx.switchTab({
                  url: '../index/index'
                })
              }
            }
          })
        }else{
          wx.showModal({
            content: result.data,
            showCancel: false
            })
        }
      }
      , false
      , function () {
        wx.hideLoading();
      });


  }
















})