let App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    nav_select: false, // 快捷导航

    indicatorDots: false, // 是否显示面板指示点
    autoplay: true, // 是否自动切换
    interval: 3000, // 自动切换时间间隔
    duration: 800, // 滑动动画时长

    currentIndex: 1, // 轮播图指针
    floorstatus: false, // 返回顶部

    detail: {}, // 信息详情信息
 
  
  },

 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let _this = this;
    // 信息id
    _this.data.id = options.article_id;
    // 获取信息信息
    _this.getarticleDetail();
  },

  onShareAppMessage() {
    let _that = this;
    return {
      title: _that.data.detail.name,
      path: 'pages/article/index?article_id=' + _that.data.id,
      desc: _that.data.detail.desctext

    }
  },
  /*分享*/
  handleTapShareButton() {
    if (!((typeof wx.canIUse === 'function') && wx.canIUse('button.open-type.share'))) {
      wx.showModal({
        title: '当前版本不支持分享按钮',
        content: '请升级至最新版本微信客户端或者点击右上角分享',
        showCancel: false
      })
    }
  },

  /**
   * 设置轮播图当前指针 数字
   */
  setCurrent: function (e) {
    this.setData({
      currentIndex: e.detail.current + 1
    });
  },




  /**
   * 获取信息信息
   */
  getarticleDetail: function() {
    let _this = this;
    App._get('api/article_detail', {
      article_id: _this.data.id
    }, function(result) {
      // 初始化信息详情数据
      let data = result.data;
      _this.setData(data);
      wx.setNavigationBarTitle({title:data.detail.name});   //动态修改标题
    });
  },

  
  /*拨打电话*/
  PhoneCall:function(event){
    let tel = event.currentTarget.dataset.tel;
    wx.makePhoneCall({
      phoneNumber: tel 
    });

  },

  /*收藏*/
  collection:function(){
    wx.showToast({
      title: "收藏成功"
    })
  },
  
  /*返回导航*/
  gohome:function(){
    wx.switchTab({
      url: '../index/index'
    })
  },

  /**
   * 返回顶部
   */
  goTop: function(t) {
    this.setData({
      scrollTop: 0
    });
  },

  /**
   * 显示/隐藏 返回顶部按钮
   */
  scroll: function(e) {
    this.setData({
      floorstatus: e.detail.scrollTop > 200
    })
  },

  

 

  /**
   * 快捷导航 显示/隐藏
   */
  commonNav: function() {
    this.setData({
      nav_select: !this.data.nav_select
    });
  },

  nav: function(e) {
    let index = e.currentTarget.dataset.index;
    "home" == index ? wx.switchTab({
      url: "../index/index"
    }) : "fenlei" == index ? wx.switchTab({
      url: "../category/index"
    }) : "profile" == index && wx.switchTab({
      url: "../user/index"
    });
  },

  
})