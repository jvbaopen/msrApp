let App = getApp();

Page({
  data: {
   
    scrollHeight: null,
    showView: false,
    arrange: "arrange",     //空为双列，默认单列

    sortType: 'all',    // 排序类型
    sortPrice: false,   // 价格从低到高

    option: {},
    list: {},
    searchName:"搜索结果",
    noList: true,
    no_more: false,

    page: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {
    let _this = this;
    console.log(option);
    if (option.search){
      let name = "'" + option.search + "'的搜索结果";
      _this.setData({ searchName: name });
      wx.setNavigationBarTitle({ title: name });   //动态修改标题
    }else{
      App._get('api/getcatetitle', {
        category_id: option.category_id || 0,
      }, function (result) {
        if (result.code == 1) {
          let data = result.data.catedata;
          _this.setData({ searchName: data.name });
          wx.setNavigationBarTitle({ title: data.name });   //动态修改标题
        }
      });
    }

    // 设置商品列表高度
    _this.setListHeight();

    // 记录option
    _this.setData({ option}, function () {
      // 获取商品列表
      _this.getGoodsList(true);
    });

  },

  /**
   * 获取信息列表
   */
  getGoodsList: function (is_super, page) {
    let _this = this;
    App._get('api/article_list', {
      page: page || 1,
      sortType: this.data.sortType,
      sortPrice: this.data.sortPrice,
      category_id: this.data.option.category_id || 0,
      search: this.data.option.search || '',
    }, function (result) {
        let resultList = result.data.list
          , dataList = _this.data.list;
        if (is_super === true || typeof dataList.data === 'undefined') {
          // typeof dataList.data === 'undefined'
          _this.setData({ list: resultList, noList: false });
        } else {
          _this.setData({ 'list.data': dataList.data.concat(resultList.data) });
        }
    });
    
    


  },

  /**
 * 设置商品列表高度
 */
  setListHeight: function () {
    let _this = this;
    wx.getSystemInfo({
      success: function (res) {
        _this.setData({
          scrollHeight: res.windowHeight - 90,
        });
      }
    });
  },

 

 

});
