let App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},    //用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取当前用户信息
    this.getUserDetail();
    this.setData({
      hasLogin: App.globalData.hasLogin      
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 获取当前用户信息
   */
  getUserDetail: function () {
    let _this = this;
    App._get('api/user_detail', {}, function (result) {
      if(result.code == -1) return;
      _this.setData(result.data);
      _this.setData({hasLogin : true});
    });
  },

  /*授权登录*/
  login: function (e) {
    let _this = this;
    if (e.detail.errMsg !== 'getUserInfo:ok') {
      return false;
    }
    wx.showLoading({ title: "正在登录", mask: true });
    // 执行微信登录
    wx.login({
      success: function (res) {
        // 发送用户信息
        App._post_form('api/user_login'
          , {
            code: res.code,
            user_info: e.detail.rawData,
            encrypted_data: e.detail.encryptedData,
            iv: e.detail.iv,
            signature: e.detail.signature
          }
          , function (result) {
            // 记录token user_id
            wx.setStorageSync('token', result.data.token);
            wx.setStorageSync('user_id', result.data.user_id);
            App.globalData.hasLogin = true;
            _this.setData({
              hasLogin: App.globalData.hasLogin
            });
    
            // 跳转回原页面
            //_this.navigateBack();
            _this.update();
          }
          , false
          , function () {
            wx.hideLoading();
          });
      }
    });
  },

})