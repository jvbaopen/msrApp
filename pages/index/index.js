let App = getApp();
let cont_time=0;//首页tab点击

Page({
  data: {
    // banner轮播组件属性
    indicatorDots: true, // 是否显示面板指示点	
    autoplay: true, // 是否自动切换
    interval: 3000, // 自动切换时间间隔
    duration: 800, // 滑动动画时长
    imgHeights: {}, // 图片的高度
    imgCurrent: {}, // 当前banne所在滑块指针
    imagesHeight : 400,
    // 页面元素
    items: {},
    newest: {},
    best: {},
    scrollTop: 0,
    circular: true, // 是否采用衔接滑动
    scrollLeft: 0,  //tab标题的滚动条位置
    current: 0,//当前选中的Tab项
    page: 1,
    index: 1,
    cont:1,
    tabid:0,
    loading:false,
    showModal: false,
    plug: [],
    remind: '加载中',
  },

  onLoad: function() {
    App.setTitle();
   
  },

  //页面却换到前台触发
  onShow:function() {
    var that = this;
    that.getDeatiData();    //获取首页详情数据
    setTimeout(function () {
      that.setData({
        remind: ''
      });
    }, 1000);
  },


  //准备就绪
  onReady: function () {
    
  
    
  },
  //下拉触底函数
  onPullDownRefresh:function(){
    var that = this;
    that.getDeatiData();    //获取首页详情数据
  },


  /**
   * 获取首页数据
   */
  /*获取首页详情数据*/
  getDeatiData: function () {
    
    //加个延迟框好看点
    wx.showToast({
      icon: 'loading',
      duration: 60000,
    })
    let _this = this;
    App._get('api/index&page='+_this.data.page, {}, function (result) { 
      _this.setData(result.data);
      wx.hideToast();
    });
  },


  /**
   * 计算图片高度
   */
  imagesHeight: function (e) {
    let imgId = e.target.dataset.id,
      itemKey = e.target.dataset.itemKey,
      ratio = e.detail.width / e.detail.height, // 宽高比
      viewHeight = 750 / ratio, // 计算的高度值
      imgHeights = this.data.imgHeights;

    // 把每一张图片的对应的高度记录到数组里
    if (typeof imgHeights[itemKey] === 'undefined') {
      imgHeights[itemKey] = {};
    }
    imgHeights[itemKey][imgId] = viewHeight;
    // 第一种方式
    let imgCurrent = this.data.imgCurrent;
    if (typeof imgCurrent[itemKey] === 'undefined') {
      imgCurrent[itemKey] = Object.keys(this.data.items[itemKey].data)[0];
    }
    this.setData({
      imgHeights,
      imgCurrent
    });
  },




  /**
   * Tab的点击切换事件
   */
  tabItemClick: function (e) {
    //防止点击过快带来的闪屏问题
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    var data = e.currentTarget.dataset;
    var that = this;
    if (cont_time) {
      if (timestamp - cont_time >= 1) {
        that.nextpic(data);
        cont_time = timestamp;
      } else {
        cont_time = timestamp;
      }
    } else {
      that.nextpic(data);
      cont_time = timestamp;
    }
    that.checkCor(e);
  },
  nextpic: function (data) {
    this.setData({
      current: data.pos,
      tabid: data.tabid
    });
  },




  //设置点击tab大于第七个是自动跳到后面
  checkCor: function (e) {
    if (this.data.current > 6) {
      this.setData({
        scrollLeft: 800
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },

  /**
  * 内容区域swiper的切换事件,手指滑屏事件
  */
  contentChange: function (e) {
    
    var that = this;
    var id = e.detail.current;
    var tabid = that.data.onement[id].id;
    this.setData({
      current: id,
      tabid: tabid,
      page: 1,
    })
    this.checkCor();
  },


  /*拨打电话*/
  PhoneCall: function (event) {
    let tel = event.currentTarget.dataset.tel;
    wx.makePhoneCall({
      phoneNumber: tel
    });

  },
  

  goTop: function(t) {
    this.setData({
      scrollTop: 0
    });
  },
  /*滚动事件*/
  scroll: function(t) {
    this.setData({
      indexSearch: t.detail.scrollTop
    }), t.detail.scrollTop > 300 ? this.setData({
      floorstatus: !0
    }) : this.setData({
      floorstatus: !1
    });
  },

  //转发
  onShareAppMessage: function() {
    return {
      title: "麻水人家",
      desc: "麻水人家，麻水商家及供求信息的集合地",
      path: "/pages/index/index"
    };
  }
});